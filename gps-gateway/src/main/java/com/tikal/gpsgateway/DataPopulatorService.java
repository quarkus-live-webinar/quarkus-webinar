package com.tikal.gpsgateway;

import io.quarkus.runtime.StartupEvent;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@ApplicationScoped
@Transactional
public class DataPopulatorService {
    private static final Logger logger = LoggerFactory.getLogger(DataPopulatorService.class);



    @ConfigProperty(name = "devicesFile",defaultValue = "devices.json")
    String devicesFile;


    void onStart(@Observes StartupEvent ev) throws IOException {
        if(Device.count()==0){
            logger.info(("Start popuating the DB"));
            populateDevices();
            logger.info(("Finished popuating the DB"));
        }else {
            logger.info("DB is already populated -> Do nothing!");
        }
    }


    void populateDevices() throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                getClass().getClassLoader().getResourceAsStream(devicesFile)))) {
            br.lines()
                    .filter(line->!line.startsWith("//"))
                    .forEach(line->new JsonObject(line)
                            .mapTo(Device.class).persist());
        }
    }
}
