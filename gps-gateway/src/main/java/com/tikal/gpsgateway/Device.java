package com.tikal.gpsgateway;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Device extends PanacheEntity {
//    imei(String), vehicleName(String)
    String imei;
    String vehicleName;

    public Device() {
    }

    public Device(String imei, String vehicleName) {
        this.imei = imei;
        this.vehicleName = vehicleName;
    }

    public String getImei() {
        return imei;
    }

    public String getVehicleName() {
        return vehicleName;
    }
}
