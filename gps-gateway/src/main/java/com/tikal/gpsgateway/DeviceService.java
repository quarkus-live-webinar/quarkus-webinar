package com.tikal.gpsgateway;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

@ApplicationScoped
@Transactional
public class DeviceService {

    public Device findDeviceByImei(String imei){
        return Device.<Device>find("imei = ?1",imei)
                .singleResultOptional()
                .orElse(null);
    }
}
