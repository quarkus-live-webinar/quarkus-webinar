package com.tikal.gpsgateway;

import io.quarkus.vertx.http.runtime.devmode.Json;
import io.smallrye.reactive.messaging.kafka.KafkaRecord;
import io.vertx.core.json.JsonObject;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/")
public class GpsResource {
    private static final Logger logger = LoggerFactory.getLogger(GpsResource.class);

    @Inject
    @Channel("gps")
    Emitter<String> emitter;



    @Inject
    DeviceService deviceService;

    @POST
    @Consumes(APPLICATION_JSON)
    public void handleGps(String gps){
        logger.debug("Got GPS {}",gps);
        JsonObject gpsJson = new JsonObject(gps);
        String imei = gpsJson.getString("imei");
        Device device = deviceService.findDeviceByImei(imei);
        if(device==null){
            logger.warn("imei {} does not exist!!!",imei);
            return;
        }
        gpsJson.put("deviceId",device.id);
        gpsJson.put("vehicleName",device.getVehicleName());
        logger.debug("Publish to Kafka {}",gpsJson);
        emitter.send(KafkaRecord.of(device.imei,gpsJson.toString()));
    }
}
