package com.tikal.gpsgateway;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
public class GpsGatewayTest {

    @Inject
    DeviceService deviceService;

    @Test
    public void testDeviceServoice(){
        String imei = UUID.randomUUID().toString();
        saveDevice(imei);
        assertNotNull(deviceService.findDeviceByImei(imei));
    }

    @Transactional
    void saveDevice(String imei) {
        new Device(imei,"Kuku").persist();
    }
}
