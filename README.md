# Quarkus Live-Coding Workshop

## Pre-Requisite
Please make sure you have the following list installed and working:
1.  Git
2. JDK 11
3. IDE for Java (InteliJ, VS-Code, Eclipse)
4. Maven (latest 3.6.3) :
5. Docker (latest 19.03.8) 
6. Docker-Compose (latest 1.25.5)
7. Any HTTP client tool - Postman, Postwoman, or cURL
8. Good and stable Internet connection

### Free Ports
Make sure the following ports are free (you should not get any line with "LISTEN") :
```console
lsof -i tcp:19092
lsof -i tcp:9092
lsof -i tcp:27017
lsof -i tcp:8080
lsof -i tcp:8081
lsof -i tcp:8082
lsof -i tcp:8083
lsof -i tcp:8084
lsof -i tcp:8085
lsof -i tcp:5432
lsof -i tcp:80
lsof -i tcp:2181
lsof -i tcp:2888
lsof -i tcp:2888
lsof -i tcp:3888
``` 

### Run Docker Compose 
Make sure all the services are running using docker-compose:
```console
docker-compose up
```

### Make sure all services are running:
```console
docker-compose ps
```

You should see the following prints, which make sure all our 8 infrastructure components are up and running
docker-compose ps    
```console
         Name                       Command               State                 Ports               
----------------------------------------------------------------------------------------------------
broker                   /etc/confluent/docker/run        Up      0.0.0.0:19092->19092/tcp, 9092/tcp
fleet_management_mongo   docker-entrypoint.sh mongod      Up      0.0.0.0:27017->27017/tcp          
geo-speed                ./application -Dquarkus.ht ...   Up      0.0.0.0:8080->8080/tcp            
gps-simulator            ./application -Dquarkus.ht ...   Up      8080/tcp, 0.0.0.0:8084->8084/tcp  
gps_gateway_db           docker-entrypoint.sh postgres    Up      0.0.0.0:5432->5432/tcp            
staff-manager            ./application -Dquarkus.ht ...   Up      8080/tcp, 0.0.0.0:8085->8085/tcp  
webapp                   nginx -g daemon off;             Up      0.0.0.0:80->80/tcp                
zookeeper                /etc/confluent/docker/run        Up      2181/tcp, 2888/tcp, 3888/tcp 
```

### Check Web-Site is Running
Open the Webapp at (http://localhost)
