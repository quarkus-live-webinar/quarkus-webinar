# Geo Speed Service

## Bootstrap

### Bootstrap the Geo-Speed project
1. Boostrap a new Quarkus-Project named geo-speed. Add the following dependencies:
`RESTEasy Jackson`
2. Add geo-service to your IDE workspace as a new module
3. Sanity:
    * Make sure you can run it with `./mvnw clean quarkus:dev` 
    and the run the following cURL command `curl http://localhost:8080/hello` 
    * which sends a request to the ExampleResource
    * Delete the ExampleResource and also META-INF generated folder
    
## Implementation 

### LocationData DTO class
Create LocationData DTO class with the following properties sentTime(String), lat (double), lon (double)

### SpeedData DTO
Create SpeedData DTO class with the following properties speed(Integer), beam(Integer)

### SpeedCalculator CDI-Bean
Make sure it's @ApplicationScope annotation.
Create SpeedCalculator CDI-Bean with ApplicationScoped which has the beam and distance methods

#### distance method
```java
public double distance(final double lat1, final double lon1, 
        final double lat2, final double lon2, final String unit) {
    final double theta = lon1 - lon2;
    double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2))
            + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) 
                * Math.cos(deg2rad(theta));
    dist = Math.acos(dist);
    dist = rad2deg(dist);
    dist = dist * 60 * 1.1515;
    if (unit == "K") {
        dist = dist * 1.609344;
    } else if (unit == "N") {
        dist = dist * 0.8684;
    }
    return (dist);
}
```
---
#### beamFromCoordinate method	
```java
public double beamFromCoordinate (final double lat1, 
        final double long1, 
            final double lat2, final double long2) {

    final double dLon = (long2 - long1);

    final double y = Math.sin(dLon) * Math.cos(lat2);
    final double x = Math.cos(lat1) * Math.sin(lat2) - 
            Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

    double brng = Math.atan2(y, x);

    brng = Math.toDegrees(brng);
    brng = (brng + 360) % 360;
    brng = 360 - brng;

    return brng;
}
```
---
#### Util methods
```java
/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
/* :: This function converts decimal degrees to radians : */
/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
private double deg2rad(final double deg) {
    return (deg * Math.PI / 180.0);
}

/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
/* :: This function converts radians to decimal degrees : */
/* ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: */
private double rad2deg(final double rad) {
    return (rad * 180 / Math.PI);
}
```

### SpeedResource (Controller)
Create SpeedResource which with an endpoint that takes array of Locations and return the SpeedData,
with the speed and beam details with the following API : `public SpeedData getSpeedData(LocationData[] locations)`

### Log
Make sure you set the log level on debug for `com.tikal` prints : `quarkus.log.category."com.tikal".level=DEBUG`

## Test

### Test with cURL Command
```console
    curl --location --request POST 'http://localhost:8080' \
    --header 'Content-Type: application/json' \
    --data-raw '[
                    {
                        "imei":"123",
                        "sentTime":"2020-03-07T10:38:45Z",
                        "lat":53.3999080,
                        "lon":-1.4105900
                    },
                    {
                        "imei":"123",
                        "sentTime":"2020-03-07T10:38:46Z",
                        "lat":53.3999085,
                        "lon":-1.4107922
                    }
                ]'
``` 

## Build + Docker-Image

### JVM vs Native
You can choose if you want to build a JVM image or Native image using GraalVM. 
Please note that building a native image takes a few minutes needs at least 8GB for the Docker env.
 
### Docker Image with JVM Image
```console
./mvnw clean install
docker build -f ./src/main/docker/Dockerfile.jvm \ 
    -t fleetmanagement/geo-speed:online-webinar .
```

### Docker Image with Native-Image (Can take a few minutes...)
```console
./mvnw clean install -Pnative -Dquarkus.native.container-build=true
docker build -f ./src/main/docker/Dockerfile.native -t fleetmanagement/geo-speed:online-webinar .
```

## Run
```console
docker run --rm --name=geo-speed --network=fleetman_network -it -p 8080:8080 fleetmanagement/geo-speed:online-webinar
````





