# Simulator

## Run Simulator

### Start Simulator
Start the simulator using the following cURL command:
```console
curl --location --request POST  \
'http://localhost:8084/restart?fleetSize=10&scheduleIntervalInMillis=500'
```
### Stop Simulator
To Stop the simulator you can use the following cURL command:
```console
curl --location --request POST  \
    'http://localhost:8084/stop'
```


### Check Web-Site is Running
Open the Webapp at (http://localhost)
