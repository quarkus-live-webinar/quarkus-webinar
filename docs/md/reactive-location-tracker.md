# Reactive Location Tracker

## Bootstrap

### Adding Quarkus dependency : quarkus-resteasy-mutiny
Before we start, we need to add `quarkus-resteasy-mutiny` as yet another dependency. 
We can either do it manually, by adding it to the pom.xml, 
or run the following command: 

```console
./mvnw quarkus:add-extension \
    -Dextensions="io.quarkus:quarkus-resteasy-mutiny"
```

## Implementation 

### SpeedService client interface
Make the REASTEasy SpeedService client interface to return Uni<SpeedData>, so the REST client call will not be blocking

### Reactive Panache - ReactivePanacheMongoEntity
Change our model - We change Location object to inherit from `ReactivePanacheMongoEntity`. 
This will make all MongoDB api be non-blocking reactive

###  GPSMessageHandler Goes Reactive
Make GPSMessageHandler handle the gps with a reactive manner: 

#### processGPS
1. First we need to change the processGPS to return a Uni<Location> instead of Location object
2. If the there is no previous location in cache, we should return `Uni.createFrom().item(currentLocation)` instead of the current location
3. Then we should change the call to the SpeedData service - now it returns the Uni<SpeedData> (instead of SpeedData)
4. We should now handle the response by `onItem().apply` on the Uni returned object.

#### processGPS
We should change the `saveLocation` method to use the Panache reactive api by subscrbing to persist api with `subscribe().with(onItemCallback,onFailureCallback)`

### LocationResource
Change the `getLocationsFor` on the class `LocationResource`: 
    Rather than return the collection of Location objects, stream the Location from the DB as Multi of Location objects using
the `stream()` api (instead of `list()` api)  



## Run & Test

### Run Quarkus
Run the application : From the gps-gateway folder run the following command : 
```console
./mvnw clean compile quarkus:dev
```

### Test Registration
2. Register to SSE using the /stream GET endpoint by running the following
 cURL api (you can also use the browser by going to this URL http://localhost:8081/stream):

```console
curl http://localhost:8081/stream
```

### Manually send Location events
Send 2 events using Kafka-Console-Producer to the same device (so we can also calculate the speed)

```console
docker run --net=fleetman_network confluentinc/cp-zookeeper:5.5.0 bash -c "echo '{\"imei\":\"913766255838676\",\"sentTime\":\"2020-07-18T16:00:00Z\",\"lat\":53.399900,\"lon\":-1.41020,\"deviceId\":1,\"vehicleName\":\"city_truck\"}' | kafka-console-producer --request-required-acks 1 --bootstrap-server broker:9092 --topic gps"
docker run --net=fleetman_network confluentinc/cp-zookeeper:5.5.0 bash -c "echo '{\"imei\":\"913766255838676\",\"sentTime\":\"2020-07-18T16:00:02Z\",\"lat\":53.399908,\"lon\":-1.41070,\"deviceId\":1,\"vehicleName\":\"city_truck\"}' | kafka-console-producer --request-required-acks 1 --bootstrap-server broker:9092 --topic gps"
```

Make sure you got the SSE with the Location data including the speed data

### Get Device History 
Get history for the device

```console
    curl 'http://localhost:8081/history?deviceId=1'
```


## Build + Docker-Image
You can choose if you want to build a JVM image or Native image using GraalVM. 
Please note that building a native image takes a few minutes needs at least 8GB for the Docker env.

### Docker Image with JVM Image

``` 
./mvnw clean install
docker build -f ./src/main/docker/Dockerfile.jvm \ 
    -t fleetmanagement/location-tracker:online-webinar .
```

### Docker Image with Native-Image

``` 
./mvnw clean install -Pnative -Dquarkus.native.container-build=true
docker build -f ./src/main/docker/Dockerfile.native \
    -t fleetmanagement/location-tracker:online-webinar .
```

##Run

```
docker run --rm --name=location-tracker \
    --network=fleetman_network  -it -p 8081:8081 \
    --env geo_speed_mp_rest_url='http://geo-speed:8080' \
    --env mp_messaging_incoming_gps_bootstrap_servers='broker:9092' \
    --env quarkus_mongodb_connection_string='mongodb://fleet_management_mongo:27017' \
    fleetmanagement/location-tracker:online-webinar
```
