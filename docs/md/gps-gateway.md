# GPS-Gateway

## Bootstrap

### Bootstrap the GPS-Service project
* Boostrap a new Quarkus-Project named gps-service. Add the following dependencies:
    * RESTEasy Jackson (Jackson serialization support for RESTEasy)
    * SmallRye Reactive Messaging - Kafka Connector (Connect to Kafka with Reactive Messaging)
    * Hibernate ORM with Panache (Simplify your persistence code for Hibernate ORM via the active record or the repository pattern)
    * JDBC Driver - PostgreSQL
    * Cache - Enable application data caching in CDI beans
    
Or simply run the following command:
```console
    mvn io.quarkus:quarkus-maven-plugin:1.6.1.Final:create \
        -DprojectGroupId=com.tikal.fleetmanagement \
        -DprojectArtifactId=gps-gateway \
        -Dextensions="quarkus-resteasy-jackson, \
                        quarkus-smallrye-reactive-messaging-kafka, \
                        quarkus-hibernate-orm-panache, \
                        quarkus-jdbc-postgresql, \
                        quarkus-cache"
```

* Add gps-gateway service to your IDE workspace as a new module

## Implementation 

### Device Entity
Create Device Entity and inherit from PanacheEntity with the following properties : `imei(String), vehicleName(String)`. 
Make sure you put JPA annotations on the Device class
You may want also to create a unique index on vehicleName

### DeviceService CDI Bean
Create a DeviceService CDI bean with `@ApplicationScoped @Transactional` 
and a method called `findDeviceByImei` which query the DB using JPQL and Panache API

### Configuration File : application.properties
Create application.properties file in resources folder with the following properties:
```properties
    # Change the default port
    quarkus.http.port=8083
    quarkus.http.test-port=9083
    quarkus.native.additional-build-args =-H:ResourceConfigurationFiles=resources-config.json 

    # Set the debug level
    quarkus.log.category."com.tikal".level=DEBUG

    # Set Quarkus peroperries for the PostgreSQL and Hibernate
    quarkus.datasource.url=jdbc:postgresql://localhost:5432/device_manager?useSSL=false&useUnicode=yes&characterEncoding=UTF-8
    quarkus.datasource.driver=org.postgresql.Driver
    quarkus.datasource.username=postgres
    quarkus.datasource.password=postgres
    quarkus.hibernate-orm.database.generation=drop-and-create
    quarkus.hibernate-orm.log.sql=true
```

### GpsGatewayTest class
Create a `GpsGatewayTest` test class , which test the `findByImei` method.
The class should be annotated as `@QuarkusTest` and call the service you defined.

### Data Population
Create yet another bean `DataPopulatorService` with `@ApplicationScoped @Transactional` which will populate the DB with devices:
1. Add the data at the end of this page as devices.json file in resources folder.
2. Create a property with `@ConfigProperty` for devicesFile. 
3. Create init method `void onStart(@Observes StartupEvent ev)` , 
which populate the db if there are no entries for devices
4. Create `populateDevices` to read the file line-by-line and populate the DB:

```java
void populateDevices() throws IOException {
    try (BufferedReader br = new BufferedReader(new InputStreamReader(
        getClass().getClassLoader().getResourceAsStream(devicesFile)))) {
            br.lines()
                .filter(line->!line.startsWith("//"))
                .forEach(line->new JsonObject(line)
                            .mapTo(Device.class).persist());
    }
}
```
   * Make sure on the logs that the SQL print the insert statements

### GpsResource (Controller)
Create `GpsResource` with the following annotation `@Path("/")`:
1. Inject an Emitter interface - We will use it to emit messages to Kafka. We also must set the channel using @Channel annotation.
Our channel name will be "gps".
2. Inject the DeviceService bean
3. Define the `public handleGps(String gps)` as POST method that consumes the gps as a JSON in the body. 
    1. The method should extract the convert the String to JsonObject and extract the imei. 
    2. Then delegate the call to DeviceService (should be injected as well) to get the Device by the imei.
    3. If the result from the DeviceService is an existing device (not null result):
        * Add deviceId and vehicleName from the fetched device into the JsonObject. We will it as the payload data to be sent to Kafka.
        * Send a KafkaRecord message to Kafka using the Emitter reactive messaging for further processing

### Kafka Configuration
Add the following to application.properties file to configure Kafka topic as an outgoing channel:
```properties
    # Kafka
    mp.messaging.outgoing.gps.connector=smallrye-kafka
    mp.messaging.outgoing.gps.bootstrap.servers=localhost:19092
    mp.messaging.outgoing.gps.topic=gps
    mp.messaging.outgoing.gps.value.serializer=org.apache.kafka.common.serialization.StringSerializer
```

### Add resources-config.json (For native image only)
Under resources folder, create `resources-config.json` file with the following content:

```json
{
  "resources": [
    {
      "pattern": ".*\\.xml$"
    },
    {
      "pattern": ".*\\.json$"
    }
  ]
}
```

## Run & Test

### Run Quarkus
Run the application : From the gps-gateway folder run the following command : 
```console
./mvnw clean compile quarkus:dev
```

### Test with cURL
Send cURL request: 
```console
  curl --location --request POST 'http://localhost:8083' \
  --header 'Content-Type: application/json' \
  --data-raw '{
    "imei":"913766255838676",
    "sentTime":"2020-07-18T16:00:00Z",
    "lat":53.3999080,
    "lon":-1.4105900
  }'
``` 

### Run Kafka Console-Consumer
```console
docker run --net=fleetman_network confluentinc/cp-zookeeper:5.5.0 kafka-console-consumer --bootstrap-server broker:9092 --topic gps
```



## Data File (devices.json) used by DataPopulator bean
```json
{"imei":"913766255838676","vehicleName":"city_truck"}
{"imei":"988605845680323","vehicleName":"round"}
{"imei":"862358902218873","vehicleName":"round_b"}
{"imei":"508077040612447","vehicleName":"elect_delivers"}
{"imei":"494673597066338","vehicleName":"express1"}
{"imei":"338093566090807","vehicleName":"factory_run_a"}
{"imei":"013760033352497","vehicleName":"factory_run_b"}
{"imei":"452725857431162","vehicleName":"factory_run_c"}
{"imei":"984577530207742","vehicleName":"factory_run_d"}
{"imei":"444832740527965","vehicleName":"factory_run_e"}
{"imei":"497218949519100","vehicleName":"factory_run_f"}
{"imei":"911558972126630","vehicleName":"factory_run_g"}
{"imei":"338322828253055","vehicleName":"factory_run_h"}
{"imei":"918140984806740","vehicleName":"truck_a"}
{"imei":"457156847551890","vehicleName":"truck_b"}
{"imei":"452514668863346","vehicleName":"deliveries_a"}
{"imei":"985602794695959","vehicleName":"deliveries_b"}
{"imei":"307951429302890","vehicleName":"london_riverside"}
{"imei":"529255881782868","vehicleName":"service_a"}
{"imei":"862999759305246","vehicleName":"service_b"}
{"imei":"498403616090945","vehicleName":"service_c"}
{"imei":"867389395377566","vehicleName":"service_d"}
{"imei":"458075835076117","vehicleName":"service_e"}
{"imei":"538570112508109","vehicleName":"service_f"}
{"imei":"014450838158369","vehicleName":"sheff_truck_a"}
{"imei":"453535471054772","vehicleName":"sheff_truck_b"}
{"imei":"016319574696155","vehicleName":"sheff_truck_c"}
{"imei":"352489936367894","vehicleName":"sheff_truck_d"}
{"imei":"303814979178934","vehicleName":"small_service1"}
{"imei":"867209770166501","vehicleName":"top_secret"}
{"imei":"530170601820020","vehicleName":"university__a"}
{"imei":"999892935417470","vehicleName":"university__b"}
{"imei":"525020362020293","vehicleName":"university__c"}
{"imei":"010288930135081","vehicleName":"university__d"}
{"imei":"350140039582168","vehicleName":"university__e"}
{"imei":"337855488230880","vehicleName":"university__f"}
{"imei":"458831911836414","vehicleName":"university__g"}
{"imei":"490596048504967","vehicleName":"village_truck"}
```

## Build + Docker-Image
You can choose if you want to build a JVM image or Native image using GraalVM. 
Please note that building a native image takes a few minutes needs at least 8GB for the Docker env. 

###  Docker Image with JVM Image
```console
./mvnw clean install
docker build -f ./src/main/docker/Dockerfile.jvm -t fleetmanagement/gps-gateway:online-webinar .
```

### Docker Image with Native-Image
```console
./mvnw clean install -Pnative -Dquarkus.native.container-build=true 
docker build -f ./src/main/docker/Dockerfile.native -t fleetmanagement/gps-gateway:online-webinar .
```

## Run
```console
docker run --rm --name=gps-gateway --network=fleetman_network  -it -p 8083:8083 \
    --env quarkus_datasource_url='jdbc:postgresql://gps_gateway_db:5432/device_manager?useSSL=false&useUnicode=yes&characterEncoding=UTF-8' \
    --env mp_messaging_outgoing_gps_bootstrap_servers='broker:9092' \
      fleetmanagement/gps-gateway:online-webinar
```


### Test With Simulator
Start the simulator using the following cURL command:

```console
curl --location --request POST  \
'http://localhost:8084/restart?fleetSize=10&scheduleIntervalInMillis=500'
```

---
### Stop Simulator
To Stop the simulator you can use the following cURL command:

```console
curl --location --request POST  \
    'http://localhost:8084/stop'
```

## Performance Tuning

### Caching with Caffeine
Improve performance with caching on the findDeviceByImei method: 
    1. Add Cache annotation `@CacheResult(cacheName = "devices-cache")` to cache the result from previous calls
    2. Add the following to application.properties file
```properties
quarkus.cache.caffeine."devices-cache".expire-after-write=30S
quarkus.cache.caffeine."devices-cache".maximum-size=1000

###
``` 

## Change to Reactive Routes
1. Add the following dependency:

```console
    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-vertx-web</artifactId>
    </dependency>
```
2. Copy the GpsResource to yet another class named `GpsReactiveRoute` and change it as follows:
    * Add `@ApplicationScoped` instead of the `@Path` on top of the class
    * Instead of `@POST @Consumes(APPLICATION_JSON)` you should put `@Route(path = "/",methods = HttpMethod.POST)`
    * The input for the methos should be changed to `RoutingContext rc`, and you should extract the body from the RoutingContext
    * The method should end by response().end() : `rc.response().end();`






